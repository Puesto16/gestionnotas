/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion;

import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class Alumnos {

    static String[] inicializarLista(int numeroAlumnos) {
        return new String[numeroAlumnos];
    }

    static String[] aniadirAlumnos(int contAlumnoIntroducido, String[] listaAlumnos) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce el nombre del alumno ");
        listaAlumnos[contAlumnoIntroducido] = teclado.next();
        return listaAlumnos;
    }

    static void listarAlumnos(String[] listaAlumnos) {
        for (int i = 0; i < listaAlumnos.length; i++) {
            System.out.println((i + 1) + ". " + listaAlumnos[i]);
        }
    }
}
